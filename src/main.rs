extern crate rand;
use rand::random;
use std::io;

fn get_guess() -> u8 {
    loop {
        println!("Enter your guess:");

        let mut guess = String::new();
        io::stdin().read_line(&mut guess);

        match guess.trim().parse::<u8>() {
            Ok(v) => return v,
            Err(e) => println!("Could not parse the number: {}", e)
        };
    }
}

fn check_guess(guess: u8, correct: u8) -> bool {
    if guess < correct {
        println!("Too low!");
        false
    } else if guess > correct {
        println!("Too high!");
        false
    } else {
        println!("You won!");
        true
    }
}

fn main() {
    println!("Welcome to the guessing game!");
    let correct = random::<u8>();

    loop {
        let guess= get_guess();
        if check_guess(guess, correct) {
            break;
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::check_guess;

    #[test]
    fn test_check_guess_to_high() {
        let guess = 12;
        let correct = 10;

        let guess_check = check_guess(guess, correct);
        assert_eq!(guess_check, false)
    }

    #[test]
    fn test_check_guess_to_low() {
        let guess = 8;
        let correct = 10;

        let guess_check = check_guess(guess, correct);
        assert_eq!(guess_check, false)
    }

    #[test]
    fn test_check_guess_correct() {
        let guess = 10;
        let correct = 10;

        let guess_check = check_guess(guess, correct);
        assert_eq!(guess_check, true)
    }
}
